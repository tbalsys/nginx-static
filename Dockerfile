FROM alpine:3 AS build

#ARG VERSION="1.19.9"
ADD https://nginx.org/download/nginx-1.19.9.tar.gz /tmp/nginx.tar.gz

RUN apk add build-base gcc linux-headers && \
    tar -C /tmp -xf /tmp/nginx.tar.gz && \
    cd /tmp/nginx-1.19.9 && \
    ./configure \
        --prefix=/ \
        --with-cc-opt="-static" \
        --with-ld-opt="-static" \
        --with-cpu-opt="generic" \
        --http-log-path="/dev/stdout" \
        --error-log-path="/dev/stderr" \
        --without-select_module \
        --without-poll_module \
        --without-http_charset_module \
        --without-http_gzip_module \
        --without-http_ssi_module \
        --without-http_userid_module \
        --without-http_access_module \
        --without-http_auth_basic_module \
        --without-http_mirror_module \
        --without-http_autoindex_module \
        --without-http_geo_module \
        --without-http_map_module \
        --without-http_split_clients_module \
        --without-http_referer_module \
        --without-http_rewrite_module \
        --without-http_proxy_module \
        --without-http_fastcgi_module \
        --without-http_uwsgi_module \
        --without-http_scgi_module \
        --without-http_grpc_module \
        --without-http_memcached_module \
        --without-http_limit_conn_module \
        --without-http_limit_req_module \
        --without-http_empty_gif_module \
        --without-http_browser_module \
        --without-http_upstream_hash_module \
        --without-http_upstream_ip_hash_module \
        --without-http_upstream_least_conn_module \
        --without-http_upstream_random_module \
        --without-http_upstream_keepalive_module \
        --without-http_upstream_zone_module \
        --without-mail_pop3_module \
        --without-mail_imap_module \
        --without-mail_smtp_module \
        --without-stream_limit_conn_module \
        --without-stream_access_module \
        --without-stream_geo_module \
        --without-stream_map_module \
        --without-stream_split_clients_module \
        --without-stream_return_module \
        --without-stream_set_module \
        --without-stream_upstream_hash_module \
        --without-stream_upstream_least_conn_module \
        --without-stream_upstream_random_module \
        --without-stream_upstream_zone_module \
        --without-pcre && \
      make && \
    mkdir -p \
      /rootfs/etc \
      /rootfs/sbin \
      /rootfs/conf \
      /rootfs/logs \
      /rootfs/html \
      /rootfs/error \
      /rootfs/client_body_temp && \
    cp /tmp/nginx-1.19.9/objs/nginx /rootfs/sbin && \
    echo "nogroup:*:65534:nobody" > /rootfs/etc/group && \
    echo "nobody:*:65534:65534:::" > /rootfs/etc/passwd

COPY error /rootfs/error/
COPY nginx.conf /rootfs/conf/

FROM scratch
COPY --from=build --chown=65534:65534 /rootfs /
USER 65534:65534
ENTRYPOINT ["/sbin/nginx"]
CMD ["-g", "daemon off;"]

