# Description

Minimal nginx image for serving static files. Image contains only the
nginx binary and some configuration files. Nginx is built from source
and includes no optional modules and minimal features.

# Usage

```
podman run --name nginx-static -v .:/html:ro -p 8000:8000 --init quay.io/tbalsys/nginx-static
```

Volume mountpoints:

- `/html`: mountpoint to serve static content from
- `/conf/nginx.conf`: mountpoint to replace the nginx configuaration

Ports:

- nginx listens on port `8000` by default

# Build

```
podman build . -t quay.io/tbalsys/nginx-static
```

